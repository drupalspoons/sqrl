<?php

namespace Drupal\sqrl;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Utility\Random;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\sqrl\Entity\IdentityInterface;
use Drupal\user\UserDataInterface;
use Drupal\user\UserInterface;

class Identities {

  use StringManipulation;

  private const DEFAULT_EMAIL_DOMAIN = '@local.localhost';

  /**
   * @var string
   */
  protected $idk;

  /**
   * @var string
   */
  protected $pidk;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * @var \Drupal\Component\Utility\Random
   */
  protected $random;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Drupal\sqrl\Log
   */
  protected $logger;

  /**
   * Identities constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\user\UserDataInterface $user_data
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\sqrl\Log $logger
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, UserDataInterface $user_data, ConfigFactoryInterface $config_factory, Log $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->userData = $user_data;
    $this->random = new Random();
    $this->config = $config_factory->get('sqrl.settings');
    $this->logger = $logger;
  }

  /**
   * @param $idk
   *
   * @return \Drupal\sqrl\Identities
   */
  public function setIdk($idk): Identities {
    $this->idk = $idk;
    return $this;
  }

  /**
   * @param $pidk
   *
   * @return \Drupal\sqrl\Identities
   */
  public function setPidk($pidk): Identities {
    $this->pidk = $pidk;
    return $this;
  }

  /**
   * @param string $key
   * @param int|null $uid
   *
   * @return \Drupal\sqrl\Entity\IdentityInterface|null
   */
  protected function getIdentity($key, $uid = NULL): ?IdentityInterface {
    if (empty($key)) {
      return NULL;
    }
    $conditions = [
      'idk' => $key,
    ];
    if ($uid !== NULL) {
      $conditions['user'] = $uid;
    }
    try {
      /** @var \Drupal\sqrl\Entity\IdentityInterface[] $identities */
      $identities = $this->entityTypeManager->getStorage('sqrl_identity')->loadByProperties($conditions);
    }
    catch (InvalidPluginDefinitionException $e) {
    }
    catch (PluginNotFoundException $e) {
    }
    if (!empty($identities)) {
      return reset($identities);
    }
    return NULL;
  }

  /**
   * @param int $uid
   *
   * @return \Drupal\sqrl\Entity\IdentityInterface[]
   */
  public function getIdentities($uid): array {
    try {
      /** @var \Drupal\sqrl\Entity\IdentityInterface[] $identities */
      $identities = $this->entityTypeManager->getStorage('sqrl_identity')->loadByProperties([
        'user' => $uid,
      ]);
    }
    catch (InvalidPluginDefinitionException $e) {
    }
    catch (PluginNotFoundException $e) {
    }
    if (!empty($identities)) {
      return $identities;
    }
    return [];
  }

  /**
   * @param int@null $uid
   *
   * @return \Drupal\sqrl\Entity\IdentityInterface|null
   */
  public function getIdentityByIdk($uid = NULL): ?IdentityInterface {
    return $this->getIdentity($this->idk, $uid);
  }

  /**
   * @param int|null $uid
   *
   * @return \Drupal\sqrl\Entity\IdentityInterface|null
   */
  public function getIdentityByPidk($uid = NULL): ?IdentityInterface {
    return $this->getIdentity($this->pidk, $uid);
  }

  /**
   * @param \Drupal\user\UserInterface $account
   */
  public function cancel(UserInterface $account): void {
    foreach ($this->getIdentities($account->id()) as $identity) {
      try {
        if ($identity->removeUser($account)) {
          $identity->save();
        }
      }
      catch (EntityStorageException $e) {
      }
    }
  }

  /**
   * @param \Drupal\sqrl\Entity\IdentityInterface $identity
   * @param string $secret
   * @param bool $encrypt
   */
  public function updatePassword(IdentityInterface $identity, $secret, $encrypt): void {
    if ($this->config->get('allow_multiple_ids_per_account') || $this->config->get('allow_multiple_accounts_per_id')) {
      // When there is NOT a 1:1-relationship between SQRL ID and user account,
      // we can't encrypt and decrypt user passwords while they should be
      // unaccessible.
      return;
    }

    $users = $identity->getUsers();
    if (count($users) !== 1) {
      // Well, something is not as it should be, let's log this as anexception.
      $this->logger->error('Identity %id should have exactly one account associated', [
        '%id' => $identity->id(),
      ]);
      return;
    }

    $ciphers = openssl_get_cipher_methods();
    $cipher = empty($ciphers) ? '' : reset($ciphers);

    $nonceSize = openssl_cipher_iv_length($cipher);
    $strong = TRUE;
    /** @noinspection CryptographicallySecureRandomnessInspection */
    $iv = openssl_random_pseudo_bytes($nonceSize, $strong);
    if ($strong === FALSE || $iv === FALSE) {
      $this->logger->error('Your systm does not produce secure randomness');
      return;
    }

    $user = reset($users);
    $name = 'enc-pw-id-' . $identity->id();

    if ($encrypt) {
      $password = openssl_encrypt(
        $user->getPassword(),
        $cipher,
        $secret,
        OPENSSL_RAW_DATA,
        $iv
      );
      $this->userData->set('sqrl', $user->id(), $name, $this->base64_encode($password));
      $user->setPassword('');
    }
    else {
      $password = openssl_decrypt(
        $this->base64_decode($this->userData->get('sqrl', $user->id(), $name)),
        $cipher,
        $secret,
        OPENSSL_RAW_DATA,
        $iv
      );
      $user->setPassword($password);
    }
    try {
      $user->save();
    }
    catch (EntityStorageException $e) {
      $this->logger->error('Unable to save user account when encrypting/decrypting password.');
    }
  }

  /**
   * @param int $uid
   *
   * @return bool
   */
  public function hasUserEnabledIdentities($uid): bool {
    try {
      return (bool) $this->entityTypeManager->getStorage('sqrl_identity')->getQuery()
        ->condition('user', $uid)
        ->condition('status', 1)
        ->notExists('sid')
        ->count()
        ->execute();
    }
    catch (InvalidPluginDefinitionException $e) {
    }
    catch (PluginNotFoundException $e) {
    }
    return FALSE;
  }

  /**
   * @return string
   */
  public function dummyMail(): string {
    return urlencode($this->random->string(15)) . self::DEFAULT_EMAIL_DOMAIN;
  }

  /**
   * @param string $mail
   *
   * @return bool
   */
  public function isDummyMail($mail): bool {
    return strpos($mail, self::DEFAULT_EMAIL_DOMAIN) !== FALSE;
  }

}
