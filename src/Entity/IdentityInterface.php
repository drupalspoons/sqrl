<?php

namespace Drupal\sqrl\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface defining a sqrl entity type.
 */
interface IdentityInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the sqrl creation timestamp.
   *
   * @return int
   *   Creation timestamp of the sqrl.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the sqrl creation timestamp.
   *
   * @param int $timestamp
   *   The sqrl creation timestamp.
   *
   * @return \Drupal\sqrl\Entity\IdentityInterface
   *   The called sqrl entity.
   */
  public function setCreatedTime($timestamp): IdentityInterface;

  /**
   * Returns the sqrl status.
   *
   * @return bool
   *   TRUE if the sqrl is enabled, FALSE otherwise.
   */
  public function isEnabled(): bool;

  /**
   * Sets the sqrl status.
   *
   * @param bool $status
   *   TRUE to enable this sqrl, FALSE to disable.
   *
   * @return \Drupal\sqrl\Entity\IdentityInterface
   *   The called sqrl entity.
   */
  public function setStatus($status): IdentityInterface;

  /**
   * @return bool
   */
  public function isSqrlOnly(): bool;

  /**
   * @param bool $flag
   *
   * @return \Drupal\sqrl\Entity\IdentityInterface
   */
  public function setSqrlOnly($flag): IdentityInterface;

  /**
   * @return bool
   */
  public function isHardLocked(): bool;

  /**
   * @param bool $flag
   *
   * @return \Drupal\sqrl\Entity\IdentityInterface
   */
  public function setHardLocked($flag): IdentityInterface;

  /**
   * @param \Drupal\sqrl\Entity\IdentityInterface $identity
   *
   * @return \Drupal\sqrl\Entity\IdentityInterface
   */
  public function setSuccessor(IdentityInterface $identity): IdentityInterface;

  /**
   * @param UserInterface $user
   *
   * @return \Drupal\sqrl\Entity\IdentityInterface
   */
  public function addUser($user): IdentityInterface;

  /**
   * @param UserInterface $user
   *
   * @return \Drupal\sqrl\Entity\IdentityInterface|null
   */
  public function removeUser($user): ?IdentityInterface;

  /**
   * @return bool
   */
  public function hasSuccessor(): bool;

  /**
   * @return \Drupal\user\UserInterface
   */
  public function getFirstUser(): UserInterface;

  /**
   * @param int $uid
   *
   * @return \Drupal\user\UserInterface|null
   */
  public function getUser($uid): ?UserInterface;

  /**
   * @return \Drupal\user\UserInterface[]
   */
  public function getUsers(): array;

  /**
   * @return string
   */
  public function getSuk(): string;

  /**
   * @return string
   */
  public function getVuk(): string;

  /**
   * @return bool
   */
  public function deleteAll(): bool;

}
