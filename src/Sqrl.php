<?php

namespace Drupal\sqrl;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * FormAlter service.
 */
class Sqrl {

  use StringManipulation;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * @var \Drupal\sqrl\Nut
   */
  protected $nut;

  /**
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * @var \Drupal\sqrl\Log
   */
  protected $log;

  protected $nutDomain;
  protected $nutBasePath = '/';

  /**
   * @var \Drupal\sqrl\Assets
   */
  protected $assets;

  /**
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * Constructs a FormAlter object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $kill_switch
   * @param \Drupal\sqrl\Assets $assets
   * @param \Drupal\sqrl\Log $log
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountProxy $current_user, ContainerInterface $container, KillSwitch $kill_switch, Assets $assets, Log $log) {
    $this->config = $config_factory->get('sqrl.settings');
    $this->currentUser = $current_user;
    $this->container = $container;
    $this->log = $log;
    $this->assets = $assets;
    $this->killSwitch = $kill_switch;
  }

  /**
   * @param \Drupal\sqrl\Nut $nut
   *
   * @return $this
   */
  public function setNut($nut): self {
    $this->nut = $nut;
    return $this;
  }

  /**
   * @return \Drupal\sqrl\Nut
   */
  public function getNut(): Nut {
    if ($this->nut === NULL) {
      $this->nut = $this->getNewNut();
    }
    return $this->nut;
  }

  /**
   * @return \Drupal\sqrl\Nut
   */
  public function getNewNut(): Nut {
    return Nut::create($this->container);
  }

  /**
   * @return string
   */
  public function getNutUrl(): string {
    $protocols = UrlHelper::getAllowedProtocols();
    $protocols[] = 'sqrl';
    UrlHelper::setAllowedProtocols($protocols);

    $url = $this->getPath('sqrl.client', [], TRUE, TRUE, TRUE);
    $parts = parse_url($url);
    $url = str_replace($parts['scheme'] . '://', 'sqrl://', $url);
    if ($parts['path'] !== '/sqrl/client') {
      // If the base_url contains a path component, then we have to append a
      // single "|" and avoid the subsequent "/" to indicate the domain string.
      $url = str_replace($parts['path'], str_replace('/sqrl', '|/sqrl', $parts['path']), $url);
      $this->nutBasePath = substr($parts['path'], 0, strpos($parts['path'], '/sqrl'));
    }

    $this->nutDomain = $parts['host'];
    if (isset($parts['port']) && $parts['port'] !== 80 && $parts['port'] !== 443) {
      $this->nutDomain .= ':' . $parts['port'];
    }
    return $url;
  }

  /**
   * @param string $route
   * @param array $parameters
   * @param bool $include_nut
   * @param bool $absolute
   * @param bool $include_can
   *
   * @return Url
   */
  public function getUrl($route, $parameters = [], $include_nut = TRUE, $absolute = TRUE, $include_can = FALSE): Url {
    $query = [];
    if ($include_nut) {
      $query['nut'] = (string) $this->nut;
    }
    if ($include_can) {
      $query['can'] = $this->base64_encode($this->getPath('sqrl.cps.url.cancel', ['token' => $this->getNut()->getCancelToken()]));
    }
    if (defined('SQRL_XDEBUG')) {
      $query['XDEBUG_SESSION_START'] = 'IDEA';
    }
    return Url::fromRoute($route, $parameters, [
      'absolute' => $absolute,
      'https' => TRUE,
      'query' => $query,
    ]);
  }

  /**
   * @param string $route
   * @param array $parameters
   * @param bool $include_nut
   * @param bool $absolute
   * @param bool $include_can
   *
   * @return string
   */
  public function getPath($route, $parameters = [], $include_nut = TRUE, $absolute = TRUE, $include_can = FALSE): string {
    return $this->getUrl($route, $parameters, $include_nut, $absolute, $include_can)->toString();
  }

  /**
   * @param string $op
   *
   * @return array
   */
  public function buildMarkup($op): array {
    $this->killSwitch->trigger();

    $this->nut = $this->getNewNut();
    $this->nut->setClientOperation($op);

    $nuturl = $this->getNutUrl();

    return [
      '#theme' => 'sqrl_widget',
      '#allowed_tags' => array_merge(Xss::getAdminTagList(), ['div', 'a', 'script', 'img']),
      '#weight' => 999,
      '#title' => $this->assets->getOperationTitle($op),
      '#operation' => $op,
      '#nuturl' => $nuturl,
      '#encodednuturl' => $this->base64_encode($nuturl),
      '#logourl' => Url::fromUserInput('/' . drupal_get_path('module', 'sqrl') . '/image/icon.png'),
      '#qrcodeurl' => $this->getPath('sqrl.img'),
      '#qrcodesize' => $this->config->get('qr_size'),
      '#description' => $this->assets->getOperationDescription($op),
      '#attached' => [
        'drupalSettings' => [
          'sqrl' => [
            'authenticated' => FALSE,
            'canceled' => FALSE,
            'debug' => $this->config->get('debug'),
            'destination' => FALSE,
            'url' => [
              'markup' => $this->getPath('sqrl.ajax', ['op' => 'markup'], FALSE),
              'poll' => $this->getPath('sqrl.ajax', ['op' => 'poll']),
            ],
            'pollIntervalInitial' => $this->config->get('poll_interval_initial') * 1000,
            'pollInterval' => $this->config->get('poll_interval') * 1000,
          ],
        ],
        'library' => [
          'sqrl/sqrl'
        ]
      ],
    ];
  }

  /**
   * @param string $op
   *
   * @return array
   */
  public function buildCachableMarkup($op): array {
    return [
      '#theme' => 'sqrl_widget_cached',
      '#title' => $this->assets->getOperationTitle($op),
      '#url' => $this->getPath('sqrl.view', ['op' => $op], FALSE),
      '#logourl' => Url::fromUserInput('/' . drupal_get_path('module', 'sqrl') . '/image/icon.png'),
      '#description' => $this->assets->getOtherDescription('click'),
      '#attached' => [
        'library' => [
          'sqrl/sqrl'
        ]
      ],
    ];
  }

}
