<?php

namespace Drupal\sqrl\Plugin\SqrlAction;

use Drupal\sqrl\SqrlActionPluginBase;

/**
 * Plugin implementation of the sqrl action "remove".
 *
 * @SqrlAction(
 *   id = "remove",
 *   label = @Translation("Remove"),
 *   description = @Translation("TBD.")
 * )
 */
class Remove extends SqrlActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function requiresSignatureRevalidation(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function run(): bool {
    if ($this->client->getIdentity() === NULL) {
      return FALSE;
    }
    return $this->client->getIdentity()->deleteAll();
  }

}
