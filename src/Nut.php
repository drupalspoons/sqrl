<?php

namespace Drupal\sqrl;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\sqrl\Exception\NutException;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Nut service.
 */
final class Nut implements ContainerInjectionInterface {

  use StringManipulation;
  use StringTranslationTrait;

  public const MODE_BUILD = 'build';
  public const MODE_FETCH = 'fetch';
  public const IS_COOKIE  = FALSE;

  public const STATUS_INITED  = 'inited';
  public const STATUS_INVALID = 'invalid';
  public const STATUS_BUILT   = 'built';
  public const STATUS_FETCHED = 'fetched';

  private $mode = self::MODE_BUILD;
  private $status = self::STATUS_INITED;
  private $expired;

  protected $nut_public;
  protected $clientTime;
  protected $clientIP;
  protected $clientOperation;
  protected $clientOperationParams = [];
  protected $clientMessages = [];
  protected $clientUid;
  protected $clientLoginToken;
  protected $clientCancelToken;

  /**
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var \Drupal\sqrl\State
   */
  protected $state;

  /**
   * @var \Drupal\sqrl\Sqrl
   */
  protected $sqrl;

  /**
   * @var \Drupal\sqrl\Log
   */
  protected $log;

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a FormAlter object.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   * @param \Drupal\Component\Datetime\TimeInterface $time
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   * @param \Drupal\sqrl\State $state
   * @param \Drupal\sqrl\Sqrl $sqrl
   * @param \Drupal\sqrl\Log $log
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   */
  public function __construct(AccountInterface $current_user, TimeInterface $time, RequestStack $request, State $state, Sqrl $sqrl, Log $log, MessengerInterface $messenger) {
    $this->currentUser = $current_user;
    $this->time = $time;
    $this->request = $request->getCurrentRequest();
    $this->state = $state;
    $this->sqrl = $sqrl;
    $this->log = $log;
    $this->messenger = $messenger;
    $this->clientLoginToken = $this->base64_encode($this->randomBytes(8));
    $this->clientCancelToken = $this->base64_encode($this->randomBytes(8));
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): Nut {
    return new static(
      $container->get('current_user'),
      $container->get('datetime.time'),
      $container->get('request_stack'),
      $container->get('sqrl.state'),
      $container->get('sqrl.handler'),
      $container->get('sqrl.log'),
      $container->get('messenger')
    );
  }

  /**
   * @param string $op
   *
   * @return \Drupal\sqrl\Nut
   */
  public function setClientOperation($op): Nut {
    $this->clientOperation = $op;
    return $this;
  }

  /**
   * @return string
   */
  public function getClientOperation(): string {
    return $this->clientOperation;
  }

  /**
   * @return string
   */
  public function getClientUid(): string {
    return $this->clientUid;
  }

  /**
   * @return string
   */
  public function getLoginToken(): string {
    return $this->clientLoginToken;
  }

  /**
   * @return string
   */
  public function getCancelToken(): string {
    return $this->clientCancelToken;
  }

  /**
   * @return string
   */
  public function getPublicNut(): string {
    $this->build();
    return $this->nut_public;
  }

  /**
   * @return bool
   */
  public function isValid(): bool {
    return ($this->status !== self::STATUS_INVALID);
  }

  /**
   * @return bool
   */
  public function isExpired(): bool {
    return $this->expired ?? FALSE;
  }

  /**
   * @return bool
   */
  public function isIpValid(): bool {
    return ($this->clientIP === $this->request->getClientIp());
  }

  /**
   * @return array
   */
  private function getParams(): array {
    return array(
      'time' => $this->time->getRequestTime(),
      'op' => $this->getClientOperation(),
      'ip' => $this->request->getClientIp(),
      'params' => $this->clientOperationParams,
      'messages to browser' => $this->clientMessages,
      'uid' => $this->currentUser->id(),
      'login token' => $this->clientLoginToken,
      'cancel token' => $this->clientCancelToken,
    );
  }

  private function build(): void {
    if ($this->mode !== self::MODE_BUILD || $this->status === self::STATUS_BUILT) {
      return;
    }
    $this->status = self::STATUS_BUILT;
    $this->nut_public = $this->base64_encode($this->randomBytes(8));

    $this->state->setNut($this->nut_public, $this->getParams());
  }

  /**
   * @return string
   */
  public function __toString() {
    return $this->getPublicNut();
  }

  public function fetch(): void {
    $this->mode = self::MODE_FETCH;
    if ($this->status === self::STATUS_FETCHED) {
      return;
    }
    try {
      $this->nut_public = $this->fetchNut();
      $this->load();
      $this->validate_expiration();
      $this->status = self::STATUS_FETCHED;
    }
    catch (NutException $e) {
      // TODO: Logging.
      $this->status = self::STATUS_INVALID;
      $this->log->debug('Fetch NUT error: ' . $e->getMessage());
    }
  }

  /**
   * @return array
   */
  public function getAccountsForSelect(): array {
    $result = [];
    foreach ($this->state->getAuth($this->getPublicNut(), FALSE) as $uid) {
      /** @var \Drupal\user\UserInterface $user */
      $user = User::load($uid);
      if ($user->isActive()) {
        $result[$uid] = $user->label();
      }
    }
    return $result;
  }

  /**
   * @param string|null $token
   *
   * @return Url|null
   */
  public function poll($token = NULL): ?Url {
    $uids = $this->state->getAuth($this->getPublicNut());
    if (!empty($uids)) {
      $op = $this->getClientOperation();
      $route = 'user.page';

      switch ($op) {
        case 'login':
        case 'register':
          /** @var \Drupal\user\UserInterface[] $users */
          $users = [];
          foreach ($uids as $uid) {
            /** @var \Drupal\user\UserInterface $user */
            $user = User::load($uid);
            if ($user->isActive()) {
              $users[] = $user;
            }
          }
          $count = count($users);

          if ($count === 0) {
            // None of the user accounts linked to the SQRL ID is active.
            $this->messenger->addError($this->t('No active user account, you can not login with this SQRL identity.'));
            return NULL;
          }
          if ($count === 1) {
            // Exactly one active user account is linked to the SQRL ID, let them log in.
            $account = reset($users);
            user_login_finalize($account);
          }
          else {
            // More than one active user accounts are linked to the SQRL ID,
            // the user needs to select which one to log into.
            $this->state->setAuth($this->getPublicNut(), $users);
            $this->sqrl->setNut($this);
            return $this->sqrl->getUrl('sqrl.ident.select', ['token' => $token]);
          }
          break;

        case 'link':
          // User linked their account to their SQRL identity.
          break;

        case 'unlink':
          // User unlinked their account from their SQRL identity.
          break;

        case 'profile':
          // More than one active user accounts are linked to the SQRL ID,
          // the user needs to select which one to log into.
          $this->state->setAuth($this->getPublicNut(), $this->getClientUid());
          $this->sqrl->setNut($this);
          return $this->sqrl->getUrl('sqrl.profile.edit', [
            'user' => $this->getClientUid(),
            'token' => $token,
          ]);
          break;

      }
      foreach ($this->state->getMessages($this->getPublicNut()) as $message) {
        $this->messenger->addMessage($message['message'], $message['type']);
      }

      return Url::fromRoute($route);
    }
    return NULL;
  }

  /**
   * @return string
   *
   * @throws NutException
   */
  private function fetchNut(): string {
    $nut = $this->request->query->get('nut');
    if (!$nut) {
      throw new NutException('Nut missing from GET request');
    }
    return $nut;
  }

  private function validate_expiration(): void {
    $this->expired = FALSE;
    if ($this->clientTime + State::EXPIRE_NUT < $this->time->getRequestTime()) {
      $this->expired = TRUE;
    }
  }

  /**
   * @throws NutException
   */
  private function load(): void {
    $params = $this->state->getNut($this->nut_public);
    if (empty($params)) {
      throw new NutException('No params received from implementing framework');
    }
    if (empty($params['time'])) {
      throw new NutException('Wrong params received from implementing framework');
    }
    if (empty($params['op'])) {
      throw new NutException('Wrong params received from implementing framework');
    }
    if (empty($params['ip'])) {
      throw new NutException('Wrong params received from implementing framework');
    }
    if (!isset($params['params']) || !is_array($params['params'])) {
      throw new NutException('Wrong params received from implementing framework');
    }
    if (!isset($params['messages to browser']) || !is_array($params['messages to browser'])) {
      throw new NutException('Wrong params received from implementing framework');
    }
    if (!isset($params['uid'])) {
      throw new NutException('Wrong params received from implementing framework');
    }
    if (!isset($params['login token'])) {
      throw new NutException('Wrong params received from implementing framework');
    }
    if (!isset($params['cancel token'])) {
      throw new NutException('Wrong params received from implementing framework');
    }

    $this->clientTime = $params['time'];
    $this->clientOperation = $params['op'];
    $this->clientIP = $params['ip'];
    $this->clientOperationParams = $params['params'];
    $this->clientMessages = $params['messages to browser'];
    $this->clientUid = $params['uid'];
    $this->clientLoginToken = $params['login token'];
    $this->clientCancelToken = $params['cancel token'];
  }

}
