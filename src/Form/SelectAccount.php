<?php

namespace Drupal\sqrl\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\sqrl\Sqrl;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a SQRL form.
 */
class SelectAccount extends FormBase {

  /**
   * @var \Drupal\sqrl\Sqrl
   */
  protected $sqrl;

  /**
   * @var \Drupal\sqrl\Nut
   */
  protected $nut;

  /**
   * @var \Drupal\sqrl\State
   */
  protected $state;

  /**
   * Link constructor.
   *
   * @param \Drupal\sqrl\Sqrl $sqrl
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   */
  public function __construct(Sqrl $sqrl, MessengerInterface $messenger) {
    $this->sqrl = $sqrl;
    $this->messenger = $messenger;

    $this->nut = $this->sqrl->getNewNut();
    $this->nut->fetch();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SelectAccount {
    return new static(
      $container->get('sqrl.handler'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'sqrl_select_account';
  }

  /**
   * @param $token
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function access($token): AccessResult {
    if ($this->nut->isValid() && $this->nut->getLoginToken() === $token) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['info'] = [
      '#markup' => $this->t('<p>You have been authenticated and your SQRL identity is associated to more than one account.</p>'),
    ];
    $form['account'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select account'),
      '#options' => $this->nut->getAccountsForSelect(),
      '#required' => TRUE,
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Login'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->state->setAuth($this->nut->getPublicNut(), $form_state->getValue('account'));
    $this->sqrl->setNut($this->nut);
    $form_state->setRedirectUrl($this->sqrl->getUrl('sqrl.cps.url.login', ['token' => $this->nut->getLoginToken()]));
  }

}
