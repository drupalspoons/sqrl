<?php

namespace Drupal\sqrl\Response;

use Endroid\QrCode\QrCode;
use Symfony\Component\HttpFoundation\Response;

/**
 * Response which is returned as the QR code image.
 */
class QrCodeResponse extends Response {

  /**
   * @var \Endroid\QrCode\QrCode
   */
  protected $qrCode;

  /**
   * @param string $content
   */
  public function setQrCodeContent($content): void {
    $this->qrCode = new QrCode($content);
  }

  /**
   * {@inheritdoc}
   */
  public function sendHeaders(): Response {
    $this->headers->set('content-type', $this->qrCode->getContentType());
    return parent::sendHeaders();
  }

  /**
   * {@inheritdoc}
   */
  public function sendContent() {
    if (!$this->qrCode) {
      return;
    }

    // Begin capturing the byte stream.
    ob_start();
    print $this->qrCode->writeString();
  }

}
