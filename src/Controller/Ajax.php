<?php

namespace Drupal\sqrl\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\SettingsCommand;

/**
 * Provides the SQRL ajax controller.
 */
class Ajax extends Base {

  /**
   * @param null $op
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function access($op = NULL): AccessResult {
    if (!$this->client->getNut()->isValid()) {
      return AccessResult::forbidden();
    }
    return AccessResult::allowedIf($op !== NULL && in_array($op, ['markup', 'poll']));
  }

  /**
   * @param $op
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function request($op): AjaxResponse {
    $response = new AjaxResponse();
    switch ($op) {
      case 'markup':
        // TODO: Return the SQRL markup and implement display in javascript.
        break;

      case 'poll':
        if ($url = $this->client->getNut()->poll()) {
          $response->addCommand(new SettingsCommand([
            'sqrl' => [
              'authenticated' => TRUE,
              'destination' => $url->toString(),
            ],
          ], TRUE));
        }
        break;

    }
    return $response;
  }

}
