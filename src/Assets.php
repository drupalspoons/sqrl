<?php

namespace Drupal\sqrl;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;

class Assets {
  use StringTranslationTrait;

  /**
   * @param string $op
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getOperationTitle($op): TranslatableMarkup {
    switch ($op) {
      case 'login':
        return $this->t('Login with SQRL');
      case 'register':
        return $this->t('Register with SQRL');
      case 'link':
        return $this->t('Link your SQRL identity');
      case 'unlink':
        return $this->t('Unlink your SQRL identity');
      case 'profile':
        return $this->t('Edit your password and email address');
    }
    return $this->t('SQRL');
  }

  /**
   * @param string $op
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getOperationDescription($op): TranslatableMarkup {
    switch ($op) {
      case 'login':
        return $this->t('Use your SQRL client with this QR code in order to login to the site.');
      case 'register':
        return $this->t('Use your SQRL client with this QR code in order to register a new account the site.');
      case 'link':
        return $this->t('Use your SQRL client with this QR code in order to link your SQRL identity with the current user account.');
      case 'unlink':
        return $this->t('Use your SQRL client with this QR code in order to unlink your SQRL identity from the current user account.');
      case 'profile':
        return $this->t('Use your SQRL client with this QR code in order to edit your password and email address.');
    }
    return $this->t('Use your SQRL client and capture the QR code.');
  }

  /**
   * @param string $type
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getOtherDescription($type): TranslatableMarkup {
    /** @noinspection DegradedSwitchInspection */
    switch ($type) {
      case 'click':
        return $this->t('Click the logo to reveice the SQRL widget.');
    }
    return $this->t('SQRL');
  }

}
