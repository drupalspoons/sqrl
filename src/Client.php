<?php

namespace Drupal\sqrl;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Utility\Random;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Session\AccountProxy;
use Drupal\sqrl\Entity\IdentityInterface;
use Drupal\sqrl\Exception\ClientException;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Exception;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Client service.
 */
class Client {

  use StringManipulation;

  public const CRLF = "\r\n";
  public const VERSION = '1';

  public const FLAG_IDK_MATCH = 0x01;
  public const FLAG_PIDK_MATCH = 0x02;
  public const FLAG_IP_MATCH = 0x04;
  public const FLAG_SQRL_DISABLED = 0x08;
  public const FLAG_FUNCTION_NOT_SUPPORTED = 0x10;
  public const FLAG_TRANSIENT_ERROR = 0x20;
  public const FLAG_COMMAND_FAILURE = 0x40;
  public const FLAG_FAILURE = 0x80;
  public const FLAG_BAD_ID_ASSOCIATION = 0x100;
  public const FLAG_ID_SUPERSEEDED = 0x200;

  private $signatureMetaData = [
    'ids' => [
      'key' => 'idk',
      'required' => TRUE,
      'validated' => FALSE,
    ],
    'pids' => [
      'key' => 'pidk',
      'required' => FALSE,
      'validated' => FALSE,
    ],
    'urs' => [
      'key' => 'vuk',
      'required' => TRUE,
      'validated' => TRUE,
    ],
  ];

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\sqrl\Identities
   */
  protected $identities;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Drupal\sqrl\State
   */
  protected $state;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * @var \Drupal\sqrl\Sqrl
   */
  protected $sqrl;

  /**
   * @var \Drupal\sqrl\Log
   */
  protected $log;

  /**
   * @var \Drupal\Component\Utility\Random
   */
  protected $random;

  /**
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * @var \Drupal\sqrl\SqrlActionPluginManager
   */
  protected $actionPluginManager;

  /**
   * @var \Drupal\sqrl\Entity\IdentityInterface
   */
  protected $identity;

  /**
   * @var \Drupal\sqrl\Nut
   */
  private $nut;

  private $postValues;
  private $clientSignatures;
  private $clientHeader;
  private $clientVars;
  private $serverVars;
  private $options;
  private $validationString;
  private $http_code = 403;
  private $tif = 0;
  private $response = [];
  private $fields = [];

  /**
   * @var \Drupal\user\UserInterface
   */
  private $account;

  /**
   * Constructs a FormAlter object.
   *
   * @param \Drupal\Core\Session\AccountProxy $current_user
   * @param \Drupal\sqrl\Sqrl $sqrl
   * @param \Drupal\sqrl\Log $log
   * @param \Drupal\Component\Datetime\TimeInterface $time
   * @param \Drupal\sqrl\SqrlActionPluginManager $sqrl_action_plugin_manager
   * @param \Drupal\sqrl\State $state
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   * @param \Drupal\sqrl\Identities $identities
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(AccountProxy $current_user, Sqrl $sqrl, Log $log, TimeInterface $time, SqrlActionPluginManager $sqrl_action_plugin_manager, State $state, RequestStack $request, EntityTypeManager $entity_type_manager, Identities $identities, ConfigFactoryInterface $config_factory) {
    $this->currentUser = $current_user;
    $this->sqrl = $sqrl;
    $this->log = $log;
    $this->actionPluginManager = $sqrl_action_plugin_manager;
    $this->time = $time;
    $this->state = $state;
    $this->entityTypeManager = $entity_type_manager;
    $this->identities = $identities;
    $this->config = $config_factory->get('sqrl.settings');


    $this->request = $request->getCurrentRequest();
    $this->random = new Random();

    $this->nut = $sqrl->getNewNut();
    $this->nut->fetch();
  }

  /**
   * https://www.grc.com/sqrl/semantics.htm: The data to be signed are the two
   * base64url encoded values of the “client=” and “server=” parameters with the
   * “server=” value concatenated to the end of the “client=” value.
   *
   * @return string
   */
  public function process(): string {
    $this->clientSignatures = [
      'ids' => $this->getPostValue('ids'),
      'pids' => $this->getPostValue('pids'),
      'urs' => $this->getPostValue('urs'),
    ];
    $this->clientHeader = [
      'host' => $this->getServerValue('HTTP_HOST'),
      'auth' => $this->getServerValue('HTTP_AUTHENTICATION'),
      'agent' => $this->getServerValue('HTTP_USER_AGENT'),
    ];
    $this->clientVars = $this->decodeParameter($this->getPostValue('client'));
    $this->serverVars = $this->decodeParameter($this->getPostValue('server'));
    $this->options = isset($this->clientVars['opt']) ? explode('~', $this->clientVars['opt']) : [];
    $this->validationString = $this->getPostValue('client', TRUE) . $this->getPostValue('server', TRUE);

    $this->identities->setIdk($this->getClientVar('idk'));
    $this->identities->setPidk($this->getClientVar('pidk'));

    try {
      $this->validate();
    }
    catch (ClientException $e) {
      $this->tif |= self::FLAG_FAILURE;
      $this->log->error($e->getMessage());
      return $this->buildResponse();
    }

    try {
      $this->validateIpAddress();
      $this->findAccount();
    }
    catch (ClientException $e) {
      $this->log->error($e->getMessage());
      return $this->buildResponse();
    }
    $this->http_code = 200;
    $command = $this->clientVars['cmd'];
    if ($command !== 'query') {
      if ($command !== 'ident' && $this->identity === NULL) {
        $this->tif |= self::FLAG_COMMAND_FAILURE;
      }
      else {
        $this->executeCommand($command);
      }
    }
    return $this->buildResponse();
  }

  /**
   * @return \Drupal\sqrl\Entity\IdentityInterface|null
   */
  public function getIdentity(): ?IdentityInterface {
    return $this->identity;
  }

  /**
   * @return \Drupal\user\UserInterface|null
   */
  public function getAccount(): ?UserInterface {
    return $this->account;
  }

  /**
   * @return \Drupal\sqrl\Nut
   */
  public function getNut(): Nut {
    return $this->nut;
  }

  /**
   * @return int
   */
  public function getTif(): int {
    return $this->tif;
  }

  /**
   * @param string $key
   *
   * @return string
   */
  public function getClientVar($key): string {
    return $this->clientVars[$key] ?? '';
  }

  /**
   * @param string $key
   *
   * @return bool
   */
  private function getClientOpt($key): bool {
    return in_array($key, $this->options, TRUE) !== FALSE;
  }

  /**
   * @param string $key
   * @param string $value
   *
   * @return \Drupal\sqrl\Client
   */
  private function setResponse($key, $value): Client {
    $this->response[$key] = $value;
    return $this;
  }

  /**
   * @throws \Drupal\sqrl\Exception\ClientException
   */
  private function validate(): void {
    $this->validateSignatures();
    $this->validateHeader();
    $this->validateClientVars();
    $this->validateServerVars();
    $this->validateNut();
  }

  /**
   * @throws \Drupal\sqrl\Exception\ClientException
   */
  private function validateSignatures(): void {
    $msg = $this->validationString;
    // Is the message present?
    if (empty($msg)) {
      throw new ClientException('Missing validation string');
    }
    if ($this->identity !== NULL && in_array($this->clientVars['cmd'], ['enable', 'remove']) !== FALSE) {
      $this->signatureMetaData['urs']['required'] = TRUE;
      $this->signatureMetaData['urs']['validated'] = FALSE;
      $this->clientVars['vuk'] = $this->identity->getVuk();
    }
    foreach ($this->signatureMetaData as $sig_key => $data) {
      if ($data['validated']) {
        continue;
      }
      $sig = $this->clientSignatures[$sig_key] ?? '';
      $pk  = $this->clientVars[$data['key']] ?? '';
      if ($data['required'] && (empty($sig) || empty($pk))) {
        // Is the signature present?
        if (empty($sig)) {
          throw new ClientException('Missing signature');
        }
        // Is the public key present?
        if (empty($pk)) {
          throw new ClientException('Missing public key');
        }
      }
      if (!empty($sig) && !empty($pk)) {
        // Validate signature
        $this->validateSignature($msg, $sig, $pk);
        $this->signatureMetaData[$sig_key]['validated'] = TRUE;
      }
    }
  }

  /**
   * Validate a specific signature.
   *
   * @param string $msg
   *  plain text of signed message
   * @param string $sig
   *  base64url-encoded signature
   * @param string $pk
   *  base64url-encoded Public Key
   *
   * @throws \Drupal\sqrl\Exception\ClientException
   */
  private function validateSignature($msg, $sig, $pk): void {
    $this->log->debug('Validate signature %values', ['%values' => json_encode([
      'msg' => $msg,
      'sig' => $sig,
      'key' => $pk,
    ], JSON_PRETTY_PRINT)]);
    if (!sodium_crypto_sign_verify_detached($this->base64_decode($sig), $msg, $this->base64_decode($pk))) {
      throw new ClientException('Invalid signature');
    }
  }

  /**
   * Validate required header values.
   */
  private function validateHeader(): void {
    // No validation required at this point.
  }

  /**
   * Validate required client values.
   *
   * @throws \Drupal\sqrl\Exception\ClientException
   */
  private function validateClientVars(): void {
    if ($this->clientVars['ver'] !== self::VERSION) {
      throw new ClientException('Unsupported version');
    }
  }

  /**
   * Validate required server values.
   */
  private function validateServerVars(): void {
    // No validation required at this point.
  }

  /**
   * Validate nut.
   *
   * @throws \Drupal\sqrl\Exception\ClientException
   */
  private function validateNut(): void {
    if (!$this->getNut()->isValid()) {
      if ($this->getNut()->isExpired()) {
        $this->tif |= self::FLAG_TRANSIENT_ERROR;
      }
      throw new ClientException('Invalid nut');
    }
  }

  /**
   * Validate IP address.
   *
   * @throws \Drupal\sqrl\Exception\ClientException
   */
  private function validateIpAddress(): void {
    if ($this->getNut()->isIpValid()) {
      $this->tif |= self::FLAG_IP_MATCH;
      return;
    }
    if ($this->getClientOpt('noiptest')) {
      return;
    }
    throw new ClientException('Invalid nut');
  }

  /**
   * @return string
   */
  private function buildResponse(): string {
    $values = [
      'ver' => self::VERSION,
      'nut' => $this->sqrl->getNut()->getPublicNut(),
      'tif' => dechex($this->tif),
      'qry' => $this->sqrl->getPath('sqrl.client', [], TRUE, FALSE),
    ];
    if ($this->identity !== NULL && $this->getClientOpt('suk')) {
      $response['suk'] = $this->identity->getSuk();
    }
    foreach ($this->response as $key => $value) {
      $values[$key] = $value;
    }

    $output = array();
    foreach ($values as $key => $value) {
      $output[] = $key . '=' . $value;
    }
    return $this->base64_encode(implode(self::CRLF, $output) . self::CRLF);
  }

  /**
   * @param string $key
   *
   * @param bool $plain
   *
   * @return string
   */
  private function getPostValue($key, $plain = FALSE): string {
    if (!isset($this->postValues)) {
      $this->postValues = [];
      foreach (explode('&', $this->request->getContent()) as $entry) {
        [$name, $value] = explode('=', $entry);
        $this->postValues[$name] = (in_array($name, ['ids', 'pids', 'urs']) === FALSE) ? $this->base64_decode($value) : $value;
      }
      $this->log->debug('POST %values', ['%values' => json_encode($this->postValues, JSON_PRETTY_PRINT)]);
    }
    $value = $this->postValues[$key] ?? '';
    if ($plain) {
      return $this->base64_encode($value);
    }
    return $value;
  }

  /**
   * Get the value of a key in the $_SERVER array or the string unknown if the
   * key is not set.
   *
   * @param string $key
   *
   * @return string
   */
  private function getServerValue($key): string {
    return $_SERVER[$key] ?? 'unknown';
  }

  /**
   * @param string $param
   *
   * @return array
   */
  private function decodeParameter($param): array {
    $values = explode(self::CRLF, $param);
    $vars = [];
    foreach ($values as $value) {
      if (!empty($value)) {
        $parts = explode('=', $value);
        $k = array_shift($parts);
        $vars[$k] = implode('=', $parts);
      }
    }
    return $vars;
  }

  /**
   * @param \Drupal\sqrl\Entity\IdentityInterface $identity
   *
   * @throws \Drupal\sqrl\Exception\ClientException
   */
  private function validateIdentity(IdentityInterface $identity): void {
    if (!$identity->isEnabled()) {
      $this->tif |= self::FLAG_SQRL_DISABLED;
      $this->setResponse('suk', $identity->getSuk());
    }
    if ($identity->hasSuccessor()) {
      $this->tif |= self::FLAG_ID_SUPERSEEDED;
      if ($this->clientVars['cmd'] !== 'query') {
        $this->tif |= self::FLAG_COMMAND_FAILURE;
      }
      throw new ClientException('SQRL identity is out of date');
    }
  }

  /**
   * @throws ClientException
   */
  private function findAccount(): void {
    if ($identity = $this->identities->getIdentityByIdk()) {
      $this->tif |= self::FLAG_IDK_MATCH;
      $this->validateIdentity($identity);
    }
    elseif ($identity = $this->identities->getIdentityByPidk()) {
      $this->tif |= self::FLAG_PIDK_MATCH;
      $this->validateIdentity($identity);
    }
    else {
      return;
    }

    $sqrlonly = $this->getClientOpt('sqrlonly');
    $hardlock = $this->getClientOpt('hardlock');
    $changed = FALSE;
    if ($sqrlonly !== $identity->isSqrlOnly()) {
      if ($ins = $this->getClientVar('ins')) {
        $identity->setSqrlOnly($sqrlonly);
        $changed = TRUE;
      }
      else {
        $this->setResponse('sin', 'drupal');
      }
    }
    if ($hardlock !== $identity->isHardLocked()) {
      if ($ins = $this->getClientVar('ins')) {
        $identity->setHardLocked($hardlock);
        $changed = TRUE;
      }
      else {
        $this->setResponse('sin', 'drupal');
      }
    }
    if ($changed) {
      try {
        $identity->save();
        $this->identities->updatePassword($identity, $this->getClientVar('ins'), $sqrlonly || $hardlock);
      }
      catch (EntityStorageException $e) {
        $this->log->error('Unable to save updated identity with changed sqrlonly or hardlock.');
      }
    }

    $this->identity = $identity;
    if ($uid = $this->nut->getClientUid()) {
      $op = $this->getNut()->getClientOperation();
      if ($op === 'link' && !$this->config->get('allow_multiple_accounts_per_id') && $identity->getUser($uid) === NULL) {
        $this->tif |= self::FLAG_BAD_ID_ASSOCIATION;
        throw new ClientException('This site does not allow multiple user accounts per ID');
      }
      if (in_array($op, ['link', 'unlink', 'profile']) !== FALSE) {
        $this->account = User::load($uid);
      }
      elseif ($account = $this->identity->getUser($uid)) {
        $this->account = $account;
      }
      else {
        $this->tif |= self::FLAG_BAD_ID_ASSOCIATION;
        throw new ClientException('SQRL ID does not match logged in user');
      }
    }
  }

  /**
   * @param string $command
   */
  private function executeCommand($command): void {
    $this->log->debug('Execute START: ' . $this->getNut()->getPublicNut());
    $this->log->debug('Client operation: ' . $this->getNut()->getClientOperation());
    try {
      $this->log->debug('Execute ' . $command);
      /** @var \Drupal\sqrl\SqrlActionInterface $plugin */
      $plugin = $this->actionPluginManager->createInstance($command);
      if ($plugin->requiresSignatureRevalidation()) {
        $this->validateSignatures();
      }
      if ($plugin->run()) {
        $this->log->debug('Command succesful');
        $plugin->rememberSuccess();
        $this->setResponse('url', $this->sqrl->getPath('sqrl.cps.url.login', ['token' => $this->getNut()->getLoginToken()]));
      }
    }
    catch (Exception $e) {
      if ($e instanceof PluginException) {
        $this->tif |= self::FLAG_FUNCTION_NOT_SUPPORTED;
      }
      $this->tif |= self::FLAG_COMMAND_FAILURE;
      $this->log->error($e->getMessage());
    }
  }

}
